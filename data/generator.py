import csv
from slugify import slugify

with open('csv/XboxOne_GameSales.csv', newline='', encoding='latin-1') as csvfile:
    x_box_one = list(csv.reader(csvfile))

formatted_data = []

for row in x_box_one[1:]:
    formatted_row = []
    formatted_row.append(row[1])
    formatted_row.append('XOne')
    formatted_row += row[2:5]
    formatted_data.append(formatted_row)

with open('csv/PS4_GamesSales.csv', newline='', encoding='latin-1') as csvfile:
    ps4 = list(csv.reader(csvfile))

for row in ps4[1:]:
    formatted_row = []
    formatted_row.append(row[0])
    formatted_row.append('PS4')
    formatted_row += row[1:4]
    formatted_data.append(formatted_row)

with open('csv/Video_Games_Sales_as_at_22_Dec_2016.csv', newline='', encoding='latin-1') as csvfile:
    all_video_games = list(csv.reader(csvfile))

for row in all_video_games[1:]:
    formatted_data.append(row[0:5])

#formatted_data[x][0] = 'Game name'
#formatted_data[x][1] = 'platform'
#formatted_data[x][2] = 'year of release'
#formatted_data[x][3] = 'genre'
#formatted_data[x][4] = 'publisher'

output = ""
output += 'PREFIX ex: <http://example.org/>\n'
output += 'PREFIX dbr: <http://dbpedia.org/resource/>\n'
output += 'PREFIX dbp: <http://dbpedia.org/property/>\n'
output += 'PREFIX dbo: <http://dbpedia.org/ontology/>\n'
output += 'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n'
output += 'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n'
output += 'PREFIX owl: <http://www.w3.org/2002/07/owl#>\n'
output += 'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>\n'
output += '\n'

output_set = set()
data_len = len(formatted_data)
i = 0
for data in formatted_data:
    output_candidate = 'ex:' + slugify(data[0], separator="_") + ' ' + 'rdf:type' + ' ' + 'dbo:VideoGame' + ' .'
    if (output_candidate not in output_set):
        output += output_candidate
        output_set.add(output_candidate)
        output += '\n'

    output_candidate = 'ex:' + slugify(data[0], separator="_") + ' ' + 'dbp:title' + ' ' + '\"' +  data[0] + '\"' + '^^xsd:string' + ' .'
    if (output_candidate not in output_set):
        output += output_candidate
        output_set.add(output_candidate)
        output += '\n'

    output_candidate = 'ex:' + slugify(data[0], separator="_") + ' ' + 'ex:releaseYear' + ' ' + '\"' +  data[2] + '\"' + '^^xsd:gYear' + ' .'
    if (output_candidate not in output_set):
        output += output_candidate
        output_set.add(output_candidate)
        output += '\n'

    output_candidate = 'ex:' + slugify(data[0], separator="_") + ' ' + 'dbo:genre' + ' ' + 'ex:' + slugify(data[3], separator="_") + ' .'
    if (output_candidate not in output_set):
        output += output_candidate
        output_set.add(output_candidate)
        output += '\n'

    output_candidate = 'ex:' + slugify(data[3], separator="_") + ' ' + 'rdfs:label' + ' "' + slugify(data[3], separator=" ") + '"^^xsd:string .'
    if (output_candidate not in output_set):
        output += output_candidate
        output_set.add(output_candidate)
        output += '\n'

    output_candidate = 'ex:' + slugify(data[0], separator="_") + ' ' + 'dbo:publisher' + ' ' + 'ex:' + slugify(data[4], separator="_") + ' .'
    if (output_candidate not in output_set):
        output += output_candidate
        output_set.add(output_candidate)
        output += '\n'

    output_candidate = 'ex:' + slugify(data[4], separator="_") + ' ' + 'rdfs:label' + ' "' + slugify(data[4], separator=" ") + '"^^xsd:string .'
    if (output_candidate not in output_set):
        output += output_candidate
        output_set.add(output_candidate)
        output += '\n'

    output_candidate = 'ex:' + slugify(data[0], separator="_") + ' ' + 'dbo:computingPlatform' + ' ' + 'ex:' + slugify(data[1], separator="_") + ' .'
    if (output_candidate not in output_set):
        output += output_candidate
        output_set.add(output_candidate)
        output += '\n'

    output_candidate = 'ex:' + slugify(data[1], separator="_") + ' ' + 'rdfs:label' + ' "' + slugify(data[1], separator=" ") + '"^^xsd:string .'
    if (output_candidate not in output_set):
        output += output_candidate
        output_set.add(output_candidate)
        output += '\n'

    #Some dbpedia object url has invalid prefixed name string like ' , : etc, so we use full http link to avoid the error
    output_candidate = 'ex:' + slugify(data[0], separator="_") + ' ' + 'ex:dbpediaObject' + ' ' + '<http://dbpedia.org/resource/' + data[0].replace(" ", "_") + '> .'
    if (output_candidate not in output_set):
        output += output_candidate
        output_set.add(output_candidate)
        output += '\n'
    
    output += '\n'

    i += 1
    print(str(i) + '/' + str(data_len))

f = open("turtle/komariani_video_games.ttl", "w")
f.write(output)
f.close()