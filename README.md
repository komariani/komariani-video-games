# Komariani Video Games
A repository for semantic web final project created by komariani group

## Run Apache Fuseki Jena
```
./apache-jena-fuseki-3.17.0/fuseki-server
```

## Turtle Data

### Description
- Turtle data placed on `./data/turtle/komariani_video_games.ttl`
- Turtle data generated from `./data/csv/PS4_GamesSales.csv`, `./data/csv/Video_Games_Sales_as_at_22_Dec_2016.csv`, `./data/csv/Video_Games_Sales_as_at_22_Dec_2016.csv`
- Generator placed on `./data/generator.py`
- Generator requirements placed on `./data/requirements.txt`

### How to run generator
```bash
cd data
python3 generator.py
```
## Game Query API

This API assume you already upload the turtle data to fuseki on dataset named `LocalVideoGames`  

### Query By Something
- Query By Exact Name   
  Given video game's name as parameter, return video games with the name exactly match the parameter (case insensitive).  
  Request: GET  
  Example:  
  ```
  http://localhost:8000/games/?exact_name=Battlefield%201
  ```
- Query By Contains Name   
  Given video game's name as parameter, return video games with the `name that contains the parameter` (case insensitive).  
  Request: GET  
  Example:  
  ```
  http://localhost:8000/games/?contains_name=Battlefield%201
  ```
- Query By Year of Release  
  Given video game's year of release as parameter, return video games with year of release exactly match the parameter.
  Example:  
  ```
  http://localhost:8000/games/?year=2015
  ```  
- Query By Exact Genre  
  Given video game's genre as parameter, return video games with the genre exactly match the parameter (case insensitive).  
  Request: GET  
  Example:  
  ```
  http://localhost:8000/games/?exact_genre=shooter
  ```
- Query By Contains Genre  
  Given video game's genre as parameter, return video games with the genre that contains the parameter (case insensitive).  
  Request: GET  
  Example:  
  ```
  http://localhost:8000/games/?genre=shooter
  ```
- Query By Exact Publisher  
  Given video game's publisher as parameter, return video games with the publisher exactly match the parameter (case insensitive).  
  Request: GET  
  Example:  
  ```
  http://localhost:8000/games/?exact_publisher=electronic%20arts
  ```
- Query By Contains Publisher  
  Given video game's publisher as parameter, return video games with the publisher that contains the parameter (case insensitive).  
  Request: GET  
  Example:  
  ```
  http://localhost:8000/games/?publisher=electronic%20arts
  ```
- Query By Exact Platform  
  Given video game's platform as parameter, return video games with the platform exactly match the parameter (case insensitive).  
  Request: GET  
  Example:  
  ```
  http://localhost:8000/games/?exact_platform=xone
  ```
- Query By Contains Platform  
  Given video game's platform as parameter, return video games with the platform that contains the parameter (case insensitive).  
  Request: GET  
  Example:  
  ```
  http://localhost:8000/games/?platform=xone
  ```

- Query By Exact Mode  
  Given video game's mode as parameter, return video games with the mode exactly match the parameter (case sensitive).  
  Request: GET  
  Example:  
  ```
  http://localhost:8000/games/?exact_mode=Multiplayer%20video%20game
  ```
- Query By Contains Mode  
  Given video game's mode as parameter, return video games with the mode that contains the parameter (case insensitive).  
  Request: GET  
  Example:  
  ```
  http://localhost:8000/games/?mode=single
  ```

- Query By Exact Developer  
  Given video game's developer as parameter, return video games with the developer exactly match the parameter (case sensitive).  
  Request: GET  
  Example:  
  ```
  http://localhost:8000/games/?exact_developer=Mistwalker
  ```
- Query By Contains Developer  
  Given video game's developer as parameter, return video games with the developer that contains the parameter (case insensitive).  
  Request: GET  
  Example:  
  ```
  http://localhost:8000/games/?developer=mist
  ```

### Pagination
There's additional parameter that can be included in the `query by something` request for pagination purpose. The parameter is `page` with default value is `1`, if you not explicitly use the `page` parameter, the default value will be used.  
Example:
- ```
  http://localhost:8000/games/?contains_name=Battlefield%201
  ```
  The results are the data in page 1

- ```
  http://localhost:8000/games/?contains_name=Battlefield%201&page=1
  ```
  The results are the data in page 1

- ```
  http://localhost:8000/games/?contains_name=Battlefield%201&page=2
  ```
  The results are the data in page 2
