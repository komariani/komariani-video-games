from django.shortcuts import render
from SPARQLWrapper import SPARQLWrapper, JSON
from django.http import JsonResponse
from slugify import slugify
import json

def index(request):
    response = {}
    return render(request, 'index.html', response)

def set_default(obj):
    if isinstance(obj, set):
        return list(obj)
    return obj

def getVideoGames(request):
    page = request.GET.get('page', '1')
    page = int(page)

    limit = 50
    offset = (page - 1) * limit

    str_limit = str(limit)
    str_offset = str(offset)

    if request.GET.get('exact_name', '') != '':
        return getVideoGamesByExactName(request, str_limit, str_offset)
    if request.GET.get('contains_name', '') != '':
        return getVideoGamesByContainsName(request, str_limit, str_offset)
    if request.GET.get('year', '') != '':
        return getVideoGamesByYear(request, str_limit, str_offset)
    if request.GET.get('genre', '') != '':
        return getVideoGamesByGenre(request, str_limit, str_offset)
    if request.GET.get('exact_genre', '') != '':
        return getVideoGamesByExactGenre(request, str_limit, str_offset)
    if request.GET.get('publisher', '') != '':
        return getVideoGamesByPublisher(request, str_limit, str_offset)
    if request.GET.get('exact_publisher', '') != '':
        return getVideoGamesByExactPublisher(request, str_limit, str_offset)
    if request.GET.get('platform', '') != '':
        return getVideoGamesByPlatform(request, str_limit, str_offset)
    if request.GET.get('exact_platform', '') != '':
        return getVideoGamesByExactPlatform(request, str_limit, str_offset)
    if request.GET.get('mode', '') != '':
        return getVideoGamesByMode(request, str_limit, str_offset)
    if request.GET.get('exact_mode', '') != '':
        return getVideoGamesByExactMode(request, str_limit, str_offset)
    if request.GET.get('developer', '') != '':
        return getVideoGamesByDeveloper(request, str_limit, str_offset)
    if request.GET.get('exact_developer', '') != '':
        return getVideoGamesByExactDeveloper(request, str_limit, str_offset)
    return JsonResponse({})


def getVideoGamesByExactName(request, limit, offset):
    sparql = SPARQLWrapper('http://localhost:3030/LocalVideoGames/query')
    name = request.GET.get('exact_name', '').lower()
    title_name = name.title()

    query = 'PREFIX ex: <http://example.org/>' + '\n' +\
        'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
        'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
        'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
        'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
        'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
        'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
        'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
        'SELECT * ' + '\n' +\
        'WHERE {' + '\n' +\
        '    SERVICE <http://localhost:3030/LocalVideoGames/query> {' + '\n' +\
        '           ?subject dbp:title "' + title_name + '"^^xsd:string .\n' +\
        '           ?subject rdf:type ?type' + ' .\n' +\
        '           ?subject ex:releaseYear ?releaseYear' + ' .\n' +\
        '           ?subject dbo:genre ?genre' +  ' .\n' +\
        '           ?subject dbo:publisher ?publisher' +  ' .\n' +\
        '           ?subject dbo:computingPlatform ?computingPlatform' +  ' .\n' +\
        '           ?subject ex:dbpediaObject ?dbpediaObject' +  ' .\n' +\
        '    }' + '\n' +\
        '    SERVICE <http://dbpedia.org/sparql> {' + '\n' +\
        '           OPTIONAL { dbr:' + title_name.replace(" ", "_") + ' dbo:abstract ?abstract' + ' .\n' +\
        '               filter(lang(?abstract) = "en") }' + '\n' +\
        '           OPTIONAL { dbr:' + title_name.replace(" ", "_") + ' dbp:modes ?mode' + ' . }\n' +\
        '           OPTIONAL { dbr:' + title_name.replace(" ", "_") + ' dbo:designer ?designer' + ' . }\n' +\
        '           OPTIONAL { dbr:' + title_name.replace(" ", "_") + ' dbo:developer ?developer' + ' . }\n' +\
        '           OPTIONAL { dbr:' + title_name.replace(" ", "_") + ' dbp:ign ?ignRating' + ' . }\n' +\
        '    }' + '\n' +\
        '}' + '\n' +\
        'ORDER BY ?subject LIMIT ' + limit + ' OFFSET ' + offset

    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    final_results = {}
    for entity in results['results']['bindings']:
        outside_key = title_name
        if not outside_key in final_results:
            final_results[outside_key] = {}
            final_results[outside_key]['title'] = title_name
        if 'type' in entity:
            final_results[outside_key]['type'] = entity['type']['value']
        if 'releaseYear' in entity:
            final_results[outside_key]['releaseYear'] = entity['releaseYear']['value']
        if 'genre' in entity:
            if not 'genre' in final_results[outside_key]:
                final_results[outside_key]['genre'] = set()
            final_results[outside_key]['genre'].add(entity['genre']['value'])
        if 'publisher' in entity:
            if not 'publisher' in final_results[outside_key]:
                final_results[outside_key]['publisher'] = set()
            final_results[outside_key]['publisher'].add(entity['publisher']['value'])
        if 'computingPlatform' in entity:
            if not 'computingPlatform' in final_results[outside_key]:
                final_results[outside_key]['computingPlatform'] = set()
            final_results[outside_key]['computingPlatform'].add(entity['computingPlatform']['value'])
        if 'dbpediaObject' in entity:
            final_results[outside_key]['dbpediaObject'] = entity['dbpediaObject']['value']
        if 'abstract' in entity:
            final_results[outside_key]['abstract'] = entity['abstract']['value']
        if 'mode' in entity:
            if not 'mode' in final_results[outside_key]:
                final_results[outside_key]['mode'] = set()
            final_results[outside_key]['mode'].add(entity['mode']['value'])
        if 'designer' in entity:
            if not 'designer' in final_results[outside_key]:
                final_results[outside_key]['designer'] = set()
            final_results[outside_key]['designer'].add(entity['designer']['value'])
        if 'developer' in entity:
            if not 'developer' in final_results[outside_key]:
                final_results[outside_key]['developer'] = set()
            final_results[outside_key]['developer'].add(entity['developer']['value'])
        if 'ignRating' in entity:
            final_results[outside_key]['ignRating'] = entity['ignRating']['value']

    final_results = json.dumps(final_results, default=set_default)
    final_results = json.loads(final_results)

    return JsonResponse(final_results)

def getVideoGamesByContainsName(request, limit, offset):
    sparql = SPARQLWrapper('http://localhost:3030/LocalVideoGames/query')
    name = request.GET.get('contains_name', '').lower()
    title_name = name.title()

    query = 'PREFIX ex: <http://example.org/>' + '\n' +\
        'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
        'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
        'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
        'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
        'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
        'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
        'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
        'SELECT * ' + '\n' +\
        'WHERE {' + '\n' +\
        '    SERVICE <http://localhost:3030/LocalVideoGames/query> {' + '\n' +\
        '           ?subject dbp:title ?title' + ' .\n' +\
        '           ?subject rdf:type ?type' + ' .\n' +\
        '           ?subject ex:releaseYear ?releaseYear' + ' .\n' +\
        '           ?subject dbo:genre ?genre' +  ' .\n' +\
        '           ?subject dbo:publisher ?publisher' +  ' .\n' +\
        '           ?subject dbo:computingPlatform ?computingPlatform' +  ' .\n' +\
        '           ?subject ex:dbpediaObject ?dbpediaObject' +  ' .\n' +\
        '           FILTER regex(lcase(str(?title)), "' + name + '")' +  '\n' +\
        '    }' + '\n' +\
        '}' + '\n' +\
        'ORDER BY ?subject LIMIT ' + limit + ' OFFSET ' + offset

    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    final_results = {}
    for entity in results['results']['bindings']:
        outside_key = entity['title']['value']
        if not outside_key in final_results:
            final_results[outside_key] = {}
            final_results[outside_key]['title'] = entity['title']['value']
        if 'type' in entity:
            final_results[outside_key]['type'] = entity['type']['value']
        if 'releaseYear' in entity:
            final_results[outside_key]['releaseYear'] = entity['releaseYear']['value']
        if 'genre' in entity:
            if not 'genre' in final_results[outside_key]:
                final_results[outside_key]['genre'] = set()
            final_results[outside_key]['genre'].add(entity['genre']['value'])
        if 'publisher' in entity:
            if not 'publisher' in final_results[outside_key]:
                final_results[outside_key]['publisher'] = set()
            final_results[outside_key]['publisher'].add(entity['publisher']['value'])
        if 'computingPlatform' in entity:
            if not 'computingPlatform' in final_results[outside_key]:
                final_results[outside_key]['computingPlatform'] = set()
            final_results[outside_key]['computingPlatform'].add(entity['computingPlatform']['value'])
        if 'dbpediaObject' in entity:
            final_results[outside_key]['dbpediaObject'] = entity['dbpediaObject']['value']
        if 'abstract' in entity:
            final_results[outside_key]['abstract'] = entity['abstract']['value']
        if 'mode' in entity:
            if not 'mode' in final_results[outside_key]:
                final_results[outside_key]['mode'] = set()
            final_results[outside_key]['mode'].add(entity['mode']['value'])
        if 'designer' in entity:
            if not 'designer' in final_results[outside_key]:
                final_results[outside_key]['designer'] = set()
            final_results[outside_key]['designer'].add(entity['designer']['value'])
        if 'developer' in entity:
            if not 'developer' in final_results[outside_key]:
                final_results[outside_key]['developer'] = set()
            final_results[outside_key]['developer'].add(entity['developer']['value'])
        if 'ignRating' in entity:
            final_results[outside_key]['ignRating'] = entity['ignRating']['value']

    for result in final_results:
        result_object = final_results[result]
        query = 'PREFIX ex: <http://example.org/>' + '\n' +\
                'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
                'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
                'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
                'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
                'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
                'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
                'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
                'SELECT * ' + '\n' +\
                'WHERE {' + '\n' +\
                '    SERVICE <http://dbpedia.org/sparql> {' + '\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:abstract ?abstract' + ' .\n' +\
                '               filter(lang(?abstract) = "en") }' + '\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbp:modes ?mode' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:designer ?designer' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:developer ?developer' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbp:ign ?ignRating' + ' . }\n' +\
                '    }' + '\n' +\
                '}'

        sparql.setQuery(query)

        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        for entity in results['results']['bindings']:
            if 'abstract' in entity:
                result_object['abstract'] = entity['abstract']['value']
            if 'mode' in entity:
                if not 'mode' in result_object:
                    result_object['mode'] = set()
                result_object['mode'].add(entity['mode']['value'])
            if 'designer' in entity:
                if not 'designer' in result_object:
                    result_object['designer'] = set()
                result_object['designer'].add(entity['designer']['value'])
            if 'developer' in entity:
                if not 'developer' in result_object:
                    result_object['developer'] = set()
                result_object['developer'].add(entity['developer']['value'])
            if 'ignRating' in entity:
                result_object['ignRating'] = entity['ignRating']['value']

    final_results = json.dumps(final_results, default=set_default)
    final_results = json.loads(final_results)

    return JsonResponse(final_results)

def getVideoGamesByYear(request, limit, offset):
    sparql = SPARQLWrapper('http://localhost:3030/LocalVideoGames/query')
    year = request.GET.get('year', '')

    query = 'PREFIX ex: <http://example.org/>' + '\n' +\
        'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
        'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
        'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
        'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
        'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
        'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
        'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
        'SELECT * ' + '\n' +\
        'WHERE {' + '\n' +\
        '    SERVICE <http://localhost:3030/LocalVideoGames/query> {' + '\n' +\
        '           ?subject dbp:title ?title' + ' .\n' +\
        '           ?subject rdf:type ?type' + ' .\n' +\
        '           ?subject ex:releaseYear "' + year + '"^^xsd:gYear .\n' +\
        '           ?subject dbo:genre ?genre' +  ' .\n' +\
        '           ?subject dbo:publisher ?publisher' +  ' .\n' +\
        '           ?subject dbo:computingPlatform ?computingPlatform' +  ' .\n' +\
        '           ?subject ex:dbpediaObject ?dbpediaObject' +  ' .\n' +\
        '    }' + '\n' +\
        '}' + '\n' +\
        'ORDER BY ?subject LIMIT ' + limit + ' OFFSET ' + offset

    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    final_results = {}
    for entity in results['results']['bindings']:
        outside_key = entity['title']['value']
        if not outside_key in final_results:
            final_results[outside_key] = {}
            final_results[outside_key]['title'] = entity['title']['value']
        if 'type' in entity:
            final_results[outside_key]['type'] = entity['type']['value']
        if not 'releaseYear' in final_results[outside_key]:
            final_results[outside_key]['releaseYear'] = year
        if 'genre' in entity:
            if not 'genre' in final_results[outside_key]:
                final_results[outside_key]['genre'] = set()
            final_results[outside_key]['genre'].add(entity['genre']['value'])
        if 'publisher' in entity:
            if not 'publisher' in final_results[outside_key]:
                final_results[outside_key]['publisher'] = set()
            final_results[outside_key]['publisher'].add(entity['publisher']['value'])
        if 'computingPlatform' in entity:
            if not 'computingPlatform' in final_results[outside_key]:
                final_results[outside_key]['computingPlatform'] = set()
            final_results[outside_key]['computingPlatform'].add(entity['computingPlatform']['value'])
        if 'dbpediaObject' in entity:
            final_results[outside_key]['dbpediaObject'] = entity['dbpediaObject']['value']
        if 'abstract' in entity:
            final_results[outside_key]['abstract'] = entity['abstract']['value']
        if 'mode' in entity:
            if not 'mode' in final_results[outside_key]:
                final_results[outside_key]['mode'] = set()
            final_results[outside_key]['mode'].add(entity['mode']['value'])
        if 'designer' in entity:
            if not 'designer' in final_results[outside_key]:
                final_results[outside_key]['designer'] = set()
            final_results[outside_key]['designer'].add(entity['designer']['value'])
        if 'developer' in entity:
            if not 'developer' in final_results[outside_key]:
                final_results[outside_key]['developer'] = set()
            final_results[outside_key]['developer'].add(entity['developer']['value'])
        if 'ignRating' in entity:
            final_results[outside_key]['ignRating'] = entity['ignRating']['value']

    for result in final_results:
        result_object = final_results[result]
        query = 'PREFIX ex: <http://example.org/>' + '\n' +\
                'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
                'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
                'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
                'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
                'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
                'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
                'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
                'SELECT * ' + '\n' +\
                'WHERE {' + '\n' +\
                '    SERVICE <http://dbpedia.org/sparql> {' + '\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:abstract ?abstract' + ' .\n' +\
                '               filter(lang(?abstract) = "en") }' + '\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbp:modes ?mode' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:designer ?designer' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:developer ?developer' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbp:ign ?ignRating' + ' . }\n' +\
                '    }' + '\n' +\
                '}'

        sparql.setQuery(query)

        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        for entity in results['results']['bindings']:
            if 'abstract' in entity:
                result_object['abstract'] = entity['abstract']['value']
            if 'mode' in entity:
                if not 'mode' in result_object:
                    result_object['mode'] = set()
                result_object['mode'].add(entity['mode']['value'])
            if 'designer' in entity:
                if not 'designer' in result_object:
                    result_object['designer'] = set()
                result_object['designer'].add(entity['designer']['value'])
            if 'developer' in entity:
                if not 'developer' in result_object:
                    result_object['developer'] = set()
                result_object['developer'].add(entity['developer']['value'])
            if 'ignRating' in entity:
                result_object['ignRating'] = entity['ignRating']['value']

    final_results = json.dumps(final_results, default=set_default)
    final_results = json.loads(final_results)

    return JsonResponse(final_results)

def getVideoGamesByExactGenre(request, limit, offset):
    sparql = SPARQLWrapper('http://localhost:3030/LocalVideoGames/query')
    genre = request.GET.get('exact_genre', '')

    query = 'PREFIX ex: <http://example.org/>' + '\n' +\
        'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
        'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
        'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
        'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
        'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
        'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
        'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
        'SELECT * ' + '\n' +\
        'WHERE {' + '\n' +\
        '    SERVICE <http://localhost:3030/LocalVideoGames/query> {' + '\n' +\
        '           ?subject dbp:title ?title' + ' .\n' +\
        '           ?subject rdf:type ?type' + ' .\n' +\
        '           ?subject ex:releaseYear ?releaseYear .\n' +\
        '           ?subject dbo:genre ex:' + slugify(genre, separator="_") +  ' .\n' +\
        '           ?subject dbo:genre ?additionalGenre' +  ' .\n' +\
        '           ?subject dbo:publisher ?publisher' +  ' .\n' +\
        '           ?subject dbo:computingPlatform ?computingPlatform' +  ' .\n' +\
        '           ?subject ex:dbpediaObject ?dbpediaObject' +  ' .\n' +\
        '    }' + '\n' +\
        '}' + '\n' +\
        'ORDER BY ?subject LIMIT ' + limit + ' OFFSET ' + offset

    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    final_results = {}
    for entity in results['results']['bindings']:
        outside_key = entity['title']['value']
        if not outside_key in final_results:
            final_results[outside_key] = {}
            final_results[outside_key]['title'] = entity['title']['value']
        if 'type' in entity:
            final_results[outside_key]['type'] = entity['type']['value']
        if 'releaseYear' in entity:
            final_results[outside_key]['releaseYear'] = entity['releaseYear']['value']
        if not 'genre' in final_results[outside_key]:
            final_results[outside_key]['genre'] = set()
            final_results[outside_key]['genre'].add('http://example.org/' + slugify(genre, separator="_"))
        if 'additionalGenre' in entity:
            final_results[outside_key]['genre'].add(entity['additionalGenre']['value'])
        if 'publisher' in entity:
            if not 'publisher' in final_results[outside_key]:
                final_results[outside_key]['publisher'] = set()
            final_results[outside_key]['publisher'].add(entity['publisher']['value'])
        if 'computingPlatform' in entity:
            if not 'computingPlatform' in final_results[outside_key]:
                final_results[outside_key]['computingPlatform'] = set()
            final_results[outside_key]['computingPlatform'].add(entity['computingPlatform']['value'])
        if 'dbpediaObject' in entity:
            final_results[outside_key]['dbpediaObject'] = entity['dbpediaObject']['value']
        if 'abstract' in entity:
            final_results[outside_key]['abstract'] = entity['abstract']['value']
        if 'mode' in entity:
            if not 'mode' in final_results[outside_key]:
                final_results[outside_key]['mode'] = set()
            final_results[outside_key]['mode'].add(entity['mode']['value'])
        if 'designer' in entity:
            if not 'designer' in final_results[outside_key]:
                final_results[outside_key]['designer'] = set()
            final_results[outside_key]['designer'].add(entity['designer']['value'])
        if 'developer' in entity:
            if not 'developer' in final_results[outside_key]:
                final_results[outside_key]['developer'] = set()
            final_results[outside_key]['developer'].add(entity['developer']['value'])
        if 'ignRating' in entity:
            final_results[outside_key]['ignRating'] = entity['ignRating']['value']

    for result in final_results:
        result_object = final_results[result]
        query = 'PREFIX ex: <http://example.org/>' + '\n' +\
                'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
                'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
                'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
                'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
                'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
                'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
                'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
                'SELECT * ' + '\n' +\
                'WHERE {' + '\n' +\
                '    SERVICE <http://dbpedia.org/sparql> {' + '\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:abstract ?abstract' + ' .\n' +\
                '               filter(lang(?abstract) = "en") }' + '\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbp:modes ?mode' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:designer ?designer' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:developer ?developer' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbp:ign ?ignRating' + ' . }\n' +\
                '    }' + '\n' +\
                '}'

        sparql.setQuery(query)

        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        for entity in results['results']['bindings']:
            if 'abstract' in entity:
                result_object['abstract'] = entity['abstract']['value']
            if 'mode' in entity:
                if not 'mode' in result_object:
                    result_object['mode'] = set()
                result_object['mode'].add(entity['mode']['value'])
            if 'designer' in entity:
                if not 'designer' in result_object:
                    result_object['designer'] = set()
                result_object['designer'].add(entity['designer']['value'])
            if 'developer' in entity:
                if not 'developer' in result_object:
                    result_object['developer'] = set()
                result_object['developer'].add(entity['developer']['value'])
            if 'ignRating' in entity:
                result_object['ignRating'] = entity['ignRating']['value']

    final_results = json.dumps(final_results, default=set_default)
    final_results = json.loads(final_results)

    return JsonResponse(final_results)

def getVideoGamesByGenre(request, limit, offset):
    sparql = SPARQLWrapper('http://localhost:3030/LocalVideoGames/query')
    genre = request.GET.get('genre', '')

    query = 'PREFIX ex: <http://example.org/>' + '\n' +\
        'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
        'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
        'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
        'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
        'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
        'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
        'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
        'SELECT * ' + '\n' +\
        'WHERE {' + '\n' +\
        '    SERVICE <http://localhost:3030/LocalVideoGames/query> {' + '\n' +\
        '           ?subject dbp:title ?title' + ' .\n' +\
        '           ?subject rdf:type ?type' + ' .\n' +\
        '           ?subject ex:releaseYear ?releaseYear .\n' +\
        '           ?subject dbo:genre ?genre' + ' .\n' +\
        '           ?genre rdfs:label ?genreLabel' +  ' .\n' +\
        '           ?subject dbo:publisher ?publisher' +  ' .\n' +\
        '           ?subject dbo:computingPlatform ?computingPlatform' +  ' .\n' +\
        '           ?subject ex:dbpediaObject ?dbpediaObject' +  ' .\n' +\
        '           FILTER regex(lcase(str(?genreLabel)), "' + slugify(genre, separator=" ") + '")' +  '\n' +\
        '    }' + '\n' +\
        '}' + '\n' +\
        'ORDER BY ?subject LIMIT ' + limit + ' OFFSET ' + offset

    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    final_results = {}
    for entity in results['results']['bindings']:
        outside_key = entity['title']['value']
        if not outside_key in final_results:
            final_results[outside_key] = {}
            final_results[outside_key]['title'] = entity['title']['value']
        if 'type' in entity:
            final_results[outside_key]['type'] = entity['type']['value']
        if 'releaseYear' in entity:
            final_results[outside_key]['releaseYear'] = entity['releaseYear']['value']
        if 'genre' in entity:
            if not 'genre' in final_results[outside_key]:
                final_results[outside_key]['genre'] = set()
            final_results[outside_key]['genre'].add(entity['genre']['value'])
        if 'publisher' in entity:
            if not 'publisher' in final_results[outside_key]:
                final_results[outside_key]['publisher'] = set()
            final_results[outside_key]['publisher'].add(entity['publisher']['value'])
        if 'computingPlatform' in entity:
            if not 'computingPlatform' in final_results[outside_key]:
                final_results[outside_key]['computingPlatform'] = set()
            final_results[outside_key]['computingPlatform'].add(entity['computingPlatform']['value'])
        if 'dbpediaObject' in entity:
            final_results[outside_key]['dbpediaObject'] = entity['dbpediaObject']['value']
        if 'abstract' in entity:
            final_results[outside_key]['abstract'] = entity['abstract']['value']
        if 'mode' in entity:
            if not 'mode' in final_results[outside_key]:
                final_results[outside_key]['mode'] = set()
            final_results[outside_key]['mode'].add(entity['mode']['value'])
        if 'designer' in entity:
            if not 'designer' in final_results[outside_key]:
                final_results[outside_key]['designer'] = set()
            final_results[outside_key]['designer'].add(entity['designer']['value'])
        if 'developer' in entity:
            if not 'developer' in final_results[outside_key]:
                final_results[outside_key]['developer'] = set()
            final_results[outside_key]['developer'].add(entity['developer']['value'])
        if 'ignRating' in entity:
            final_results[outside_key]['ignRating'] = entity['ignRating']['value']

    for result in final_results:
        result_object = final_results[result]
        query = 'PREFIX ex: <http://example.org/>' + '\n' +\
                'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
                'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
                'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
                'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
                'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
                'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
                'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
                'SELECT * ' + '\n' +\
                'WHERE {' + '\n' +\
                '    SERVICE <http://dbpedia.org/sparql> {' + '\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:abstract ?abstract' + ' .\n' +\
                '               filter(lang(?abstract) = "en") }' + '\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbp:modes ?mode' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:designer ?designer' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:developer ?developer' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbp:ign ?ignRating' + ' . }\n' +\
                '    }' + '\n' +\
                '}'

        sparql.setQuery(query)

        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        for entity in results['results']['bindings']:
            if 'abstract' in entity:
                result_object['abstract'] = entity['abstract']['value']
            if 'mode' in entity:
                if not 'mode' in result_object:
                    result_object['mode'] = set()
                result_object['mode'].add(entity['mode']['value'])
            if 'designer' in entity:
                if not 'designer' in result_object:
                    result_object['designer'] = set()
                result_object['designer'].add(entity['designer']['value'])
            if 'developer' in entity:
                if not 'developer' in result_object:
                    result_object['developer'] = set()
                result_object['developer'].add(entity['developer']['value'])
            if 'ignRating' in entity:
                result_object['ignRating'] = entity['ignRating']['value']

    final_results = json.dumps(final_results, default=set_default)
    final_results = json.loads(final_results)

    return JsonResponse(final_results)

def getVideoGamesByExactPublisher(request, limit, offset):
    sparql = SPARQLWrapper('http://localhost:3030/LocalVideoGames/query')
    publisher = request.GET.get('exact_publisher', '')

    query = 'PREFIX ex: <http://example.org/>' + '\n' +\
        'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
        'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
        'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
        'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
        'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
        'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
        'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
        'SELECT * ' + '\n' +\
        'WHERE {' + '\n' +\
        '    SERVICE <http://localhost:3030/LocalVideoGames/query> {' + '\n' +\
        '           ?subject dbp:title ?title' + ' .\n' +\
        '           ?subject rdf:type ?type' + ' .\n' +\
        '           ?subject ex:releaseYear ?releaseYear .\n' +\
        '           ?subject dbo:genre ?genre' + ' .\n' +\
        '           ?subject dbo:publisher ex:' + slugify(publisher, separator="_") + ' .\n' +\
        '           ?subject dbo:publisher ?additionalPublisher' + ' .\n' +\
        '           ?subject dbo:computingPlatform ?computingPlatform' +  ' .\n' +\
        '           ?subject ex:dbpediaObject ?dbpediaObject' +  ' .\n' +\
        '    }' + '\n' +\
        '}' + '\n' +\
        'ORDER BY ?subject LIMIT ' + limit + ' OFFSET ' + offset

    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    final_results = {}
    for entity in results['results']['bindings']:
        outside_key = entity['title']['value']
        if not outside_key in final_results:
            final_results[outside_key] = {}
            final_results[outside_key]['title'] = entity['title']['value']
        if 'type' in entity:
            final_results[outside_key]['type'] = entity['type']['value']
        if 'releaseYear' in entity:
            final_results[outside_key]['releaseYear'] = entity['releaseYear']['value']
        if 'genre' in entity:
            if not 'genre' in final_results[outside_key]:
                final_results[outside_key]['genre'] = set()
            final_results[outside_key]['genre'].add(entity['genre']['value'])
        if not 'publisher' in final_results[outside_key]:
            final_results[outside_key]['publisher'] = set()
            final_results[outside_key]['publisher'].add('http://example.org/' + slugify(publisher, separator="_"))
        if 'additionalPublisher' in entity:
            final_results[outside_key]['publisher'].add(entity['additionalPublisher']['value'])
        if 'computingPlatform' in entity:
            if not 'computingPlatform' in final_results[outside_key]:
                final_results[outside_key]['computingPlatform'] = set()
            final_results[outside_key]['computingPlatform'].add(entity['computingPlatform']['value'])
        if 'dbpediaObject' in entity:
            final_results[outside_key]['dbpediaObject'] = entity['dbpediaObject']['value']
        if 'abstract' in entity:
            final_results[outside_key]['abstract'] = entity['abstract']['value']
        if 'mode' in entity:
            if not 'mode' in final_results[outside_key]:
                final_results[outside_key]['mode'] = set()
            final_results[outside_key]['mode'].add(entity['mode']['value'])
        if 'designer' in entity:
            if not 'designer' in final_results[outside_key]:
                final_results[outside_key]['designer'] = set()
            final_results[outside_key]['designer'].add(entity['designer']['value'])
        if 'developer' in entity:
            if not 'developer' in final_results[outside_key]:
                final_results[outside_key]['developer'] = set()
            final_results[outside_key]['developer'].add(entity['developer']['value'])
        if 'ignRating' in entity:
            final_results[outside_key]['ignRating'] = entity['ignRating']['value']

    for result in final_results:
        result_object = final_results[result]
        query = 'PREFIX ex: <http://example.org/>' + '\n' +\
                'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
                'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
                'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
                'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
                'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
                'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
                'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
                'SELECT * ' + '\n' +\
                'WHERE {' + '\n' +\
                '    SERVICE <http://dbpedia.org/sparql> {' + '\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:abstract ?abstract' + ' .\n' +\
                '               filter(lang(?abstract) = "en") }' + '\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbp:modes ?mode' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:designer ?designer' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:developer ?developer' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbp:ign ?ignRating' + ' . }\n' +\
                '    }' + '\n' +\
                '}'

        sparql.setQuery(query)

        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        for entity in results['results']['bindings']:
            if 'abstract' in entity:
                result_object['abstract'] = entity['abstract']['value']
            if 'mode' in entity:
                if not 'mode' in result_object:
                    result_object['mode'] = set()
                result_object['mode'].add(entity['mode']['value'])
            if 'designer' in entity:
                if not 'designer' in result_object:
                    result_object['designer'] = set()
                result_object['designer'].add(entity['designer']['value'])
            if 'developer' in entity:
                if not 'developer' in result_object:
                    result_object['developer'] = set()
                result_object['developer'].add(entity['developer']['value'])
            if 'ignRating' in entity:
                result_object['ignRating'] = entity['ignRating']['value']

    final_results = json.dumps(final_results, default=set_default)
    final_results = json.loads(final_results)

    return JsonResponse(final_results)

def getVideoGamesByPublisher(request, limit, offset):
    sparql = SPARQLWrapper('http://localhost:3030/LocalVideoGames/query')
    publisher = request.GET.get('publisher', '')

    query = 'PREFIX ex: <http://example.org/>' + '\n' +\
        'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
        'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
        'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
        'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
        'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
        'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
        'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
        'SELECT * ' + '\n' +\
        'WHERE {' + '\n' +\
        '    SERVICE <http://localhost:3030/LocalVideoGames/query> {' + '\n' +\
        '           ?subject dbp:title ?title' + ' .\n' +\
        '           ?subject rdf:type ?type' + ' .\n' +\
        '           ?subject ex:releaseYear ?releaseYear .\n' +\
        '           ?subject dbo:genre ?genre' + ' .\n' +\
        '           ?subject dbo:publisher ?publisher' + ' .\n' +\
        '           ?publisher rdfs:label ?publisherLabel' + ' .\n' +\
        '           ?subject dbo:computingPlatform ?computingPlatform' +  ' .\n' +\
        '           ?subject ex:dbpediaObject ?dbpediaObject' +  ' .\n' +\
        '           FILTER regex(lcase(str(?publisherLabel)), "' + slugify(publisher, separator=" ") + '")' +  '\n' +\
        '    }' + '\n' +\
        '}' + '\n' +\
        'ORDER BY ?subject LIMIT ' + limit + ' OFFSET ' + offset

    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    final_results = {}
    for entity in results['results']['bindings']:
        outside_key = entity['title']['value']
        if not outside_key in final_results:
            final_results[outside_key] = {}
            final_results[outside_key]['title'] = entity['title']['value']
        if 'type' in entity:
            final_results[outside_key]['type'] = entity['type']['value']
        if 'releaseYear' in entity:
            final_results[outside_key]['releaseYear'] = entity['releaseYear']['value']
        if 'genre' in entity:
            if not 'genre' in final_results[outside_key]:
                final_results[outside_key]['genre'] = set()
            final_results[outside_key]['genre'].add(entity['genre']['value'])
        if 'publisher' in entity:
            if not 'publisher' in final_results[outside_key]:
                final_results[outside_key]['publisher'] = set()
            final_results[outside_key]['publisher'].add(entity['publisher']['value'])
        if 'computingPlatform' in entity:
            if not 'computingPlatform' in final_results[outside_key]:
                final_results[outside_key]['computingPlatform'] = set()
            final_results[outside_key]['computingPlatform'].add(entity['computingPlatform']['value'])
        if 'dbpediaObject' in entity:
            final_results[outside_key]['dbpediaObject'] = entity['dbpediaObject']['value']
        if 'abstract' in entity:
            final_results[outside_key]['abstract'] = entity['abstract']['value']
        if 'mode' in entity:
            if not 'mode' in final_results[outside_key]:
                final_results[outside_key]['mode'] = set()
            final_results[outside_key]['mode'].add(entity['mode']['value'])
        if 'designer' in entity:
            if not 'designer' in final_results[outside_key]:
                final_results[outside_key]['designer'] = set()
            final_results[outside_key]['designer'].add(entity['designer']['value'])
        if 'developer' in entity:
            if not 'developer' in final_results[outside_key]:
                final_results[outside_key]['developer'] = set()
            final_results[outside_key]['developer'].add(entity['developer']['value'])
        if 'ignRating' in entity:
            final_results[outside_key]['ignRating'] = entity['ignRating']['value']

    for result in final_results:
        result_object = final_results[result]
        query = 'PREFIX ex: <http://example.org/>' + '\n' +\
                'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
                'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
                'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
                'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
                'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
                'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
                'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
                'SELECT * ' + '\n' +\
                'WHERE {' + '\n' +\
                '    SERVICE <http://dbpedia.org/sparql> {' + '\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:abstract ?abstract' + ' .\n' +\
                '               filter(lang(?abstract) = "en") }' + '\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbp:modes ?mode' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:designer ?designer' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:developer ?developer' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbp:ign ?ignRating' + ' . }\n' +\
                '    }' + '\n' +\
                '}'

        sparql.setQuery(query)

        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        for entity in results['results']['bindings']:
            if 'abstract' in entity:
                result_object['abstract'] = entity['abstract']['value']
            if 'mode' in entity:
                if not 'mode' in result_object:
                    result_object['mode'] = set()
                result_object['mode'].add(entity['mode']['value'])
            if 'designer' in entity:
                if not 'designer' in result_object:
                    result_object['designer'] = set()
                result_object['designer'].add(entity['designer']['value'])
            if 'developer' in entity:
                if not 'developer' in result_object:
                    result_object['developer'] = set()
                result_object['developer'].add(entity['developer']['value'])
            if 'ignRating' in entity:
                result_object['ignRating'] = entity['ignRating']['value']

    final_results = json.dumps(final_results, default=set_default)
    final_results = json.loads(final_results)

    return JsonResponse(final_results)

def getVideoGamesByExactPlatform(request, limit, offset):
    sparql = SPARQLWrapper('http://localhost:3030/LocalVideoGames/query')
    platform = request.GET.get('exact_platform', '')

    query = 'PREFIX ex: <http://example.org/>' + '\n' +\
        'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
        'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
        'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
        'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
        'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
        'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
        'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
        'SELECT * ' + '\n' +\
        'WHERE {' + '\n' +\
        '    SERVICE <http://localhost:3030/LocalVideoGames/query> {' + '\n' +\
        '           ?subject dbp:title ?title' + ' .\n' +\
        '           ?subject rdf:type ?type' + ' .\n' +\
        '           ?subject ex:releaseYear ?releaseYear .\n' +\
        '           ?subject dbo:genre ?genre' + ' .\n' +\
        '           ?subject dbo:publisher ?publisher' + ' .\n' +\
        '           ?subject dbo:computingPlatform ex:' + slugify(platform, separator="_") + ' .\n' +\
        '           ?subject dbo:computingPlatform ?additionalPlatform' + ' .\n' +\
        '           ?subject ex:dbpediaObject ?dbpediaObject' +  ' .\n' +\
        '    }' + '\n' +\
        '}' + '\n' +\
        'ORDER BY ?subject LIMIT ' + limit + ' OFFSET ' + offset

    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    final_results = {}
    for entity in results['results']['bindings']:
        outside_key = entity['title']['value']
        if not outside_key in final_results:
            final_results[outside_key] = {}
            final_results[outside_key]['title'] = entity['title']['value']
        if 'type' in entity:
            final_results[outside_key]['type'] = entity['type']['value']
        if 'releaseYear' in entity:
            final_results[outside_key]['releaseYear'] = entity['releaseYear']['value']
        if 'genre' in entity:
            if not 'genre' in final_results[outside_key]:
                final_results[outside_key]['genre'] = set()
            final_results[outside_key]['genre'].add(entity['genre']['value'])
        if 'publisher' in entity:
            if not 'publisher' in final_results[outside_key]:
                final_results[outside_key]['publisher'] = set()
            final_results[outside_key]['publisher'].add(entity['publisher']['value'])
        if not 'computingPlatform' in final_results[outside_key]:
            final_results[outside_key]['computingPlatform'] = set()
            final_results[outside_key]['computingPlatform'].add('http://example.org/' + slugify(platform, separator="_"))
        if 'additionalPlatform' in entity:
            final_results[outside_key]['computingPlatform'].add(entity['additionalPlatform']['value'])
        if 'dbpediaObject' in entity:
            final_results[outside_key]['dbpediaObject'] = entity['dbpediaObject']['value']
        if 'abstract' in entity:
            final_results[outside_key]['abstract'] = entity['abstract']['value']
        if 'mode' in entity:
            if not 'mode' in final_results[outside_key]:
                final_results[outside_key]['mode'] = set()
            final_results[outside_key]['mode'].add(entity['mode']['value'])
        if 'designer' in entity:
            if not 'designer' in final_results[outside_key]:
                final_results[outside_key]['designer'] = set()
            final_results[outside_key]['designer'].add(entity['designer']['value'])
        if 'developer' in entity:
            if not 'developer' in final_results[outside_key]:
                final_results[outside_key]['developer'] = set()
            final_results[outside_key]['developer'].add(entity['developer']['value'])
        if 'ignRating' in entity:
            final_results[outside_key]['ignRating'] = entity['ignRating']['value']

    for result in final_results:
        result_object = final_results[result]
        query = 'PREFIX ex: <http://example.org/>' + '\n' +\
                'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
                'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
                'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
                'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
                'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
                'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
                'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
                'SELECT * ' + '\n' +\
                'WHERE {' + '\n' +\
                '    SERVICE <http://dbpedia.org/sparql> {' + '\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:abstract ?abstract' + ' .\n' +\
                '               filter(lang(?abstract) = "en") }' + '\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbp:modes ?mode' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:designer ?designer' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:developer ?developer' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbp:ign ?ignRating' + ' . }\n' +\
                '    }' + '\n' +\
                '}'

        sparql.setQuery(query)

        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        for entity in results['results']['bindings']:
            if 'abstract' in entity:
                result_object['abstract'] = entity['abstract']['value']
            if 'mode' in entity:
                if not 'mode' in result_object:
                    result_object['mode'] = set()
                result_object['mode'].add(entity['mode']['value'])
            if 'designer' in entity:
                if not 'designer' in result_object:
                    result_object['designer'] = set()
                result_object['designer'].add(entity['designer']['value'])
            if 'developer' in entity:
                if not 'developer' in result_object:
                    result_object['developer'] = set()
                result_object['developer'].add(entity['developer']['value'])
            if 'ignRating' in entity:
                result_object['ignRating'] = entity['ignRating']['value']

    final_results = json.dumps(final_results, default=set_default)
    final_results = json.loads(final_results)

    return JsonResponse(final_results)

def getVideoGamesByPlatform(request, limit, offset):
    sparql = SPARQLWrapper('http://localhost:3030/LocalVideoGames/query')
    platform = request.GET.get('platform', '')

    query = 'PREFIX ex: <http://example.org/>' + '\n' +\
        'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
        'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
        'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
        'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
        'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
        'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
        'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
        'SELECT * ' + '\n' +\
        'WHERE {' + '\n' +\
        '    SERVICE <http://localhost:3030/LocalVideoGames/query> {' + '\n' +\
        '           ?subject dbp:title ?title' + ' .\n' +\
        '           ?subject rdf:type ?type' + ' .\n' +\
        '           ?subject ex:releaseYear ?releaseYear .\n' +\
        '           ?subject dbo:genre ?genre' + ' .\n' +\
        '           ?subject dbo:publisher ?publisher' + ' .\n' +\
        '           ?subject dbo:computingPlatform ?computingPlatform' + ' .\n' +\
        '           ?computingPlatform rdfs:label ?computingPlatformLabel' + ' .\n' +\
        '           ?subject ex:dbpediaObject ?dbpediaObject' +  ' .\n' +\
        '           FILTER regex(lcase(str(?computingPlatformLabel)), "' + slugify(platform, separator=" ") + '")' +  '\n' +\
        '    }' + '\n' +\
        '}' + '\n' +\
        'ORDER BY ?subject LIMIT ' + limit + ' OFFSET ' + offset

    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    final_results = {}
    for entity in results['results']['bindings']:
        outside_key = entity['title']['value']
        if not outside_key in final_results:
            final_results[outside_key] = {}
            final_results[outside_key]['title'] = entity['title']['value']
        if 'type' in entity:
            final_results[outside_key]['type'] = entity['type']['value']
        if 'releaseYear' in entity:
            final_results[outside_key]['releaseYear'] = entity['releaseYear']['value']
        if 'genre' in entity:
            if not 'genre' in final_results[outside_key]:
                final_results[outside_key]['genre'] = set()
            final_results[outside_key]['genre'].add(entity['genre']['value'])
        if 'publisher' in entity:
            if not 'publisher' in final_results[outside_key]:
                final_results[outside_key]['publisher'] = set()
            final_results[outside_key]['publisher'].add(entity['publisher']['value'])
        if 'computingPlatform' in entity:
            if not 'computingPlatform' in final_results[outside_key]:
                final_results[outside_key]['computingPlatform'] = set()
            final_results[outside_key]['computingPlatform'].add(entity['computingPlatform']['value'])
        if 'dbpediaObject' in entity:
            final_results[outside_key]['dbpediaObject'] = entity['dbpediaObject']['value']
        if 'abstract' in entity:
            final_results[outside_key]['abstract'] = entity['abstract']['value']
        if 'mode' in entity:
            if not 'mode' in final_results[outside_key]:
                final_results[outside_key]['mode'] = set()
            final_results[outside_key]['mode'].add(entity['mode']['value'])
        if 'designer' in entity:
            if not 'designer' in final_results[outside_key]:
                final_results[outside_key]['designer'] = set()
            final_results[outside_key]['designer'].add(entity['designer']['value'])
        if 'developer' in entity:
            if not 'developer' in final_results[outside_key]:
                final_results[outside_key]['developer'] = set()
            final_results[outside_key]['developer'].add(entity['developer']['value'])
        if 'ignRating' in entity:
            final_results[outside_key]['ignRating'] = entity['ignRating']['value']

    for result in final_results:
        result_object = final_results[result]
        query = 'PREFIX ex: <http://example.org/>' + '\n' +\
                'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
                'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
                'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
                'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
                'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
                'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
                'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
                'SELECT * ' + '\n' +\
                'WHERE {' + '\n' +\
                '    SERVICE <http://dbpedia.org/sparql> {' + '\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:abstract ?abstract' + ' .\n' +\
                '               filter(lang(?abstract) = "en") }' + '\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbp:modes ?mode' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:designer ?designer' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbo:developer ?developer' + ' . }\n' +\
                '           OPTIONAL { <' + result_object['dbpediaObject'] + '>' + ' dbp:ign ?ignRating' + ' . }\n' +\
                '    }' + '\n' +\
                '}'

        sparql.setQuery(query)

        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        for entity in results['results']['bindings']:
            if 'abstract' in entity:
                result_object['abstract'] = entity['abstract']['value']
            if 'mode' in entity:
                if not 'mode' in result_object:
                    result_object['mode'] = set()
                result_object['mode'].add(entity['mode']['value'])
            if 'designer' in entity:
                if not 'designer' in result_object:
                    result_object['designer'] = set()
                result_object['designer'].add(entity['designer']['value'])
            if 'developer' in entity:
                if not 'developer' in result_object:
                    result_object['developer'] = set()
                result_object['developer'].add(entity['developer']['value'])
            if 'ignRating' in entity:
                result_object['ignRating'] = entity['ignRating']['value']

    final_results = json.dumps(final_results, default=set_default)
    final_results = json.loads(final_results)

    return JsonResponse(final_results)

def getVideoGamesByExactMode(request, limit, offset):
    sparql = SPARQLWrapper('http://localhost:3030/LocalVideoGames/query')
    mode = request.GET.get('exact_mode', '')

    query = 'PREFIX ex: <http://example.org/>' + '\n' +\
            'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
            'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
            'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
            'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
            'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
            'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
            'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
            'SELECT * ' + '\n' +\
            'WHERE {' + '\n' +\
            '    SERVICE <http://dbpedia.org/sparql> {' + '\n' +\
            '           ?subject' + ' rdfs:label ?title' + ' .\n' +\
            '               filter(lang(?title) = "en")' + '\n' +\
            '           OPTIONAL { ?subject' + ' dbo:abstract ?abstract' + ' .\n' +\
            '               filter(lang(?abstract) = "en") }' + '\n' +\
            '           ?subject' + ' dbp:modes <http://dbpedia.org/resource/' + mode.replace(" ", "_") + '> .\n' +\
            '           OPTIONAL { ?subject' + ' dbp:modes ?additionalMode' + ' . }\n' +\
            '           OPTIONAL { ?subject' + ' dbo:designer ?designer' + ' . }\n' +\
            '           OPTIONAL { ?subject' + ' dbo:developer ?developer' + ' . }\n' +\
            '           OPTIONAL { ?subject' + ' dbp:ign ?ignRating' + ' . }\n' +\
            '    }' + '\n' +\
            '}' + '\n' +\
            'ORDER BY ?subject LIMIT ' + limit + ' OFFSET ' + offset

    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    final_results = {}
    for entity in results['results']['bindings']:
        outside_key = entity['title']['value']
        if not outside_key in final_results:
            final_results[outside_key] = {}
            final_results[outside_key]['title'] = entity['title']['value']
        if 'abstract' in entity:
            final_results[outside_key]['abstract'] = entity['abstract']['value']
        if not 'mode' in final_results[outside_key]:
            final_results[outside_key]['mode'] = set()
            final_results[outside_key]['mode'].add('http://dbpedia.org/resource/' + mode.replace(" ", "_"))
        if 'additionalMode' in entity:
            final_results[outside_key]['mode'].add(entity['additionalMode']['value'])
        if 'designer' in entity:
            if not 'designer' in final_results[outside_key]:
                final_results[outside_key]['designer'] = set()
            final_results[outside_key]['designer'].add(entity['designer']['value'])
        if 'developer' in entity:
            if not 'developer' in final_results[outside_key]:
                final_results[outside_key]['developer'] = set()
            final_results[outside_key]['developer'].add(entity['developer']['value'])
        if 'ignRating' in entity:
            final_results[outside_key]['ignRating'] = entity['ignRating']['value']

    final_results_filtered = {}
    for result in final_results:
        result_object = final_results[result]

        query = 'PREFIX ex: <http://example.org/>' + '\n' +\
            'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
            'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
            'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
            'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
            'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
            'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
            'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
            'SELECT * ' + '\n' +\
            'WHERE {' + '\n' +\
            '    SERVICE <http://localhost:3030/LocalVideoGames/query> {' + '\n' +\
            '           ?subject dbp:title "' +  result_object['title'] + '"^^xsd:string .\n' +\
            '           ?subject rdf:type ?type' + ' .\n' +\
            '           ?subject ex:releaseYear ?releaseYear .\n' +\
            '           ?subject dbo:genre ?genre' + ' .\n' +\
            '           ?subject dbo:publisher ?publisher' + ' .\n' +\
            '           ?subject dbo:computingPlatform ?computingPlatform' + ' .\n' +\
            '           ?subject ex:dbpediaObject ?dbpediaObject' +  ' .\n' +\
            '    }' + '\n' +\
            '}'

        sparql.setQuery(query)

        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        for entity in results['results']['bindings']:
            if 'type' in entity:
                result_object['type'] = entity['type']['value']
            if 'releaseYear' in entity:
                result_object['releaseYear'] = entity['releaseYear']['value']
            if 'genre' in entity:
                if not 'genre' in result_object:
                    result_object['genre'] = set()
                result_object['genre'].add(entity['genre']['value'])
            if 'publisher' in entity:
                if not 'publisher' in result_object:
                    result_object['publisher'] = set()
                result_object['publisher'].add(entity['publisher']['value'])
            if 'computingPlatform' in entity:
                if not 'computingPlatform' in result_object:
                    result_object['computingPlatform'] = set()
                result_object['computingPlatform'].add(entity['computingPlatform']['value'])
            if 'dbpediaObject' in entity:
                result_object['dbpediaObject'] = entity['dbpediaObject']['value']

        if 'type' in result_object:
            final_results_filtered[result] = final_results[result]

    final_results_filtered = json.dumps(final_results_filtered, default=set_default)
    final_results_filtered = json.loads(final_results_filtered)

    return JsonResponse(final_results_filtered)

def getVideoGamesByMode(request, limit, offset):
    sparql = SPARQLWrapper('http://localhost:3030/LocalVideoGames/query')
    mode = request.GET.get('mode', '')

    query = 'PREFIX ex: <http://example.org/>' + '\n' +\
            'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
            'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
            'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
            'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
            'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
            'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
            'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
            'SELECT * ' + '\n' +\
            'WHERE {' + '\n' +\
            '    SERVICE <http://dbpedia.org/sparql> {' + '\n' +\
            '           ?subject' + ' rdfs:label ?title' + ' .\n' +\
            '               filter(lang(?title) = "en")' + '\n' +\
            '           OPTIONAL { ?subject' + ' dbo:abstract ?abstract' + ' .\n' +\
            '               filter(lang(?abstract) = "en") }' + '\n' +\
            '           ?subject' + ' dbp:modes ?modes' + ' .\n' +\
            '           ?modes' + ' rdfs:label ?modeLabel' + ' .\n' +\
            '               filter(lang(?modeLabel) = "en")' + '\n' +\
            '           OPTIONAL { ?subject' + ' dbp:modes ?additionalMode' + ' . }\n' +\
            '           OPTIONAL { ?subject' + ' dbo:designer ?designer' + ' . }\n' +\
            '           OPTIONAL { ?subject' + ' dbo:developer ?developer' + ' . }\n' +\
            '           OPTIONAL { ?subject' + ' dbp:ign ?ignRating' + ' . }\n' +\
            '           FILTER regex(lcase(str(?modeLabel)), "' + slugify(mode, separator=" ") + '")' +  '\n' +\
            '    }' + '\n' +\
            '}' + '\n' +\
            'ORDER BY ?subject LIMIT ' + limit + ' OFFSET ' + offset

    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    final_results = {}
    for entity in results['results']['bindings']:
        outside_key = entity['title']['value']
        if not outside_key in final_results:
            final_results[outside_key] = {}
            final_results[outside_key]['title'] = entity['title']['value']
        if 'abstract' in entity:
            final_results[outside_key]['abstract'] = entity['abstract']['value']
        if not 'mode' in final_results[outside_key]:
            final_results[outside_key]['mode'] = set()
            final_results[outside_key]['mode'].add(entity['modes']['value'])
        if 'additionalMode' in entity:
            final_results[outside_key]['mode'].add(entity['additionalMode']['value'])
        if 'designer' in entity:
            if not 'designer' in final_results[outside_key]:
                final_results[outside_key]['designer'] = set()
            final_results[outside_key]['designer'].add(entity['designer']['value'])
        if 'developer' in entity:
            if not 'developer' in final_results[outside_key]:
                final_results[outside_key]['developer'] = set()
            final_results[outside_key]['developer'].add(entity['developer']['value'])
        if 'ignRating' in entity:
            final_results[outside_key]['ignRating'] = entity['ignRating']['value']

    final_results_filtered = {}
    for result in final_results:
        result_object = final_results[result]

        query = 'PREFIX ex: <http://example.org/>' + '\n' +\
            'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
            'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
            'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
            'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
            'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
            'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
            'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
            'SELECT * ' + '\n' +\
            'WHERE {' + '\n' +\
            '    SERVICE <http://localhost:3030/LocalVideoGames/query> {' + '\n' +\
            '           ?subject dbp:title "' +  result_object['title'] + '"^^xsd:string .\n' +\
            '           ?subject rdf:type ?type' + ' .\n' +\
            '           ?subject ex:releaseYear ?releaseYear .\n' +\
            '           ?subject dbo:genre ?genre' + ' .\n' +\
            '           ?subject dbo:publisher ?publisher' + ' .\n' +\
            '           ?subject dbo:computingPlatform ?computingPlatform' + ' .\n' +\
            '           ?subject ex:dbpediaObject ?dbpediaObject' +  ' .\n' +\
            '    }' + '\n' +\
            '}'

        sparql.setQuery(query)

        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        for entity in results['results']['bindings']:
            if 'type' in entity:
                result_object['type'] = entity['type']['value']
            if 'releaseYear' in entity:
                result_object['releaseYear'] = entity['releaseYear']['value']
            if 'genre' in entity:
                if not 'genre' in result_object:
                    result_object['genre'] = set()
                result_object['genre'].add(entity['genre']['value'])
            if 'publisher' in entity:
                if not 'publisher' in result_object:
                    result_object['publisher'] = set()
                result_object['publisher'].add(entity['publisher']['value'])
            if 'computingPlatform' in entity:
                if not 'computingPlatform' in result_object:
                    result_object['computingPlatform'] = set()
                result_object['computingPlatform'].add(entity['computingPlatform']['value'])
            if 'dbpediaObject' in entity:
                result_object['dbpediaObject'] = entity['dbpediaObject']['value']

        if 'type' in result_object:
            final_results_filtered[result] = final_results[result]

    final_results_filtered = json.dumps(final_results_filtered, default=set_default)
    final_results_filtered = json.loads(final_results_filtered)

    return JsonResponse(final_results_filtered)

def getVideoGamesByExactDeveloper(request, limit, offset):
    sparql = SPARQLWrapper('http://localhost:3030/LocalVideoGames/query')
    developer = request.GET.get('exact_developer', '')

    query = 'PREFIX ex: <http://example.org/>' + '\n' +\
            'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
            'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
            'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
            'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
            'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
            'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
            'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
            'SELECT * ' + '\n' +\
            'WHERE {' + '\n' +\
            '    SERVICE <http://dbpedia.org/sparql> {' + '\n' +\
            '           ?subject' + ' rdfs:label ?title' + ' .\n' +\
            '               filter(lang(?title) = "en")' + '\n' +\
            '           OPTIONAL { ?subject' + ' dbo:abstract ?abstract' + ' .\n' +\
            '               filter(lang(?abstract) = "en") }' + '\n' +\
            '           OPTIONAL { ?subject' + ' dbp:modes ?mode' + ' . }\n' +\
            '           OPTIONAL { ?subject' + ' dbo:designer ?designer' + ' . }\n' +\
            '           ?subject' + ' dbo:developer <http://dbpedia.org/resource/' + developer.replace(" ", "_") + '> .\n' +\
            '           OPTIONAL { ?subject' + ' dbo:developer ?additionalDeveloper' + ' . }\n' +\
            '           OPTIONAL { ?subject' + ' dbp:ign ?ignRating' + ' . }\n' +\
            '    }' + '\n' +\
            '}' + '\n' +\
            'ORDER BY ?subject LIMIT ' + limit + ' OFFSET ' + offset

    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    final_results = {}
    for entity in results['results']['bindings']:
        outside_key = entity['title']['value']
        if not outside_key in final_results:
            final_results[outside_key] = {}
            final_results[outside_key]['title'] = entity['title']['value']
        if 'abstract' in entity:
            final_results[outside_key]['abstract'] = entity['abstract']['value']
        if 'mode' in entity:
            if not 'mode' in final_results[outside_key]:
                final_results[outside_key]['mode'] = set()
            final_results[outside_key]['mode'].add(entity['mode']['value'])
        if 'designer' in entity:
            if not 'designer' in final_results[outside_key]:
                final_results[outside_key]['designer'] = set()
            final_results[outside_key]['designer'].add(entity['designer']['value'])
        if not 'developer' in final_results[outside_key]:
            final_results[outside_key]['developer'] = set()
            final_results[outside_key]['developer'].add('http://dbpedia.org/resource/' + developer.replace(" ", "_"))
        if 'additionalDeveloper' in entity:
            final_results[outside_key]['developer'].add(entity['additionalDeveloper']['value'])
        if 'ignRating' in entity:
            final_results[outside_key]['ignRating'] = entity['ignRating']['value']

    final_results_filtered = {}
    for result in final_results:
        result_object = final_results[result]

        query = 'PREFIX ex: <http://example.org/>' + '\n' +\
            'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
            'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
            'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
            'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
            'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
            'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
            'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
            'SELECT * ' + '\n' +\
            'WHERE {' + '\n' +\
            '    SERVICE <http://localhost:3030/LocalVideoGames/query> {' + '\n' +\
            '           ?subject dbp:title "' +  result_object['title'] + '"^^xsd:string .\n' +\
            '           ?subject rdf:type ?type' + ' .\n' +\
            '           ?subject ex:releaseYear ?releaseYear .\n' +\
            '           ?subject dbo:genre ?genre' + ' .\n' +\
            '           ?subject dbo:publisher ?publisher' + ' .\n' +\
            '           ?subject dbo:computingPlatform ?computingPlatform' + ' .\n' +\
            '           ?subject ex:dbpediaObject ?dbpediaObject' +  ' .\n' +\
            '    }' + '\n' +\
            '}'

        sparql.setQuery(query)

        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        for entity in results['results']['bindings']:
            if 'type' in entity:
                result_object['type'] = entity['type']['value']
            if 'releaseYear' in entity:
                result_object['releaseYear'] = entity['releaseYear']['value']
            if 'genre' in entity:
                if not 'genre' in result_object:
                    result_object['genre'] = set()
                result_object['genre'].add(entity['genre']['value'])
            if 'publisher' in entity:
                if not 'publisher' in result_object:
                    result_object['publisher'] = set()
                result_object['publisher'].add(entity['publisher']['value'])
            if 'computingPlatform' in entity:
                if not 'computingPlatform' in result_object:
                    result_object['computingPlatform'] = set()
                result_object['computingPlatform'].add(entity['computingPlatform']['value'])
            if 'dbpediaObject' in entity:
                result_object['dbpediaObject'] = entity['dbpediaObject']['value']

        if 'type' in result_object:
            final_results_filtered[result] = final_results[result]

    final_results_filtered = json.dumps(final_results_filtered, default=set_default)
    final_results_filtered = json.loads(final_results_filtered)

    return JsonResponse(final_results_filtered)

def getVideoGamesByDeveloper(request, limit, offset):
    sparql = SPARQLWrapper('http://localhost:3030/LocalVideoGames/query')
    developer = request.GET.get('developer', '')

    query = 'PREFIX ex: <http://example.org/>' + '\n' +\
            'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
            'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
            'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
            'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
            'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
            'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
            'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
            'SELECT * ' + '\n' +\
            'WHERE {' + '\n' +\
            '    SERVICE <http://dbpedia.org/sparql> {' + '\n' +\
            '           ?subject' + ' rdfs:label ?title' + ' .\n' +\
            '               filter(lang(?title) = "en")' + '\n' +\
            '           OPTIONAL { ?subject' + ' dbo:abstract ?abstract' + ' .\n' +\
            '               filter(lang(?abstract) = "en") }' + '\n' +\
            '           OPTIONAL { ?subject' + ' dbp:modes ?modes' + ' . }\n' +\
            '           OPTIONAL { ?subject' + ' dbo:designer ?designer' + ' . }\n' +\
            '           ?subject' + ' dbo:developer ?developer' + ' .\n' +\
            '           ?developer' + ' rdfs:label ?developerLabel' + ' .\n' +\
            '               filter(lang(?developerLabel) = "en")' + '\n' +\
            '           OPTIONAL { ?subject' + ' dbo:developer ?additionalDeveloper' + ' . }\n' +\
            '           OPTIONAL { ?subject' + ' dbp:ign ?ignRating' + ' . }\n' +\
            '           FILTER regex(lcase(str(?developerLabel)), "' + slugify(developer, separator=" ") + '")' +  '\n' +\
            '    }' + '\n' +\
            '}' + '\n' +\
            'ORDER BY ?subject LIMIT ' + limit + ' OFFSET ' + offset

    print(query)
    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    final_results = {}
    for entity in results['results']['bindings']:
        outside_key = entity['title']['value']
        if not outside_key in final_results:
            final_results[outside_key] = {}
            final_results[outside_key]['title'] = entity['title']['value']
        if 'abstract' in entity:
            final_results[outside_key]['abstract'] = entity['abstract']['value']
        if 'mode' in entity:
            if not 'mode' in final_results[outside_key]:
                final_results[outside_key]['mode'] = set()
            final_results[outside_key]['mode'].add(entity['mode']['value'])
        if 'designer' in entity:
            if not 'designer' in final_results[outside_key]:
                final_results[outside_key]['designer'] = set()
            final_results[outside_key]['designer'].add(entity['designer']['value'])
        if not 'developer' in final_results[outside_key]:
            final_results[outside_key]['developer'] = set()
            final_results[outside_key]['developer'].add(entity['developer']['value'])
        if 'additionalDeveloper' in entity:
            final_results[outside_key]['developer'].add(entity['additionalDeveloper']['value'])
        if 'ignRating' in entity:
            final_results[outside_key]['ignRating'] = entity['ignRating']['value']

    final_results_filtered = {}
    for result in final_results:
        result_object = final_results[result]

        query = 'PREFIX ex: <http://example.org/>' + '\n' +\
            'PREFIX dbr: <http://dbpedia.org/resource/>' + '\n' +\
            'PREFIX dbp: <http://dbpedia.org/property/>' + '\n' +\
            'PREFIX dbo: <http://dbpedia.org/ontology/>' + '\n' +\
            'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>' + '\n' +\
            'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' + '\n' +\
            'PREFIX owl: <http://www.w3.org/2002/07/owl#>' + '\n' +\
            'PREFIX xsd: <http://www.w3.org/2001/XMLSchema>' + '\n' +\
            'SELECT * ' + '\n' +\
            'WHERE {' + '\n' +\
            '    SERVICE <http://localhost:3030/LocalVideoGames/query> {' + '\n' +\
            '           ?subject dbp:title "' +  result_object['title'] + '"^^xsd:string .\n' +\
            '           ?subject rdf:type ?type' + ' .\n' +\
            '           ?subject ex:releaseYear ?releaseYear .\n' +\
            '           ?subject dbo:genre ?genre' + ' .\n' +\
            '           ?subject dbo:publisher ?publisher' + ' .\n' +\
            '           ?subject dbo:computingPlatform ?computingPlatform' + ' .\n' +\
            '           ?subject ex:dbpediaObject ?dbpediaObject' +  ' .\n' +\
            '    }' + '\n' +\
            '}'

        sparql.setQuery(query)

        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        for entity in results['results']['bindings']:
            if 'type' in entity:
                result_object['type'] = entity['type']['value']
            if 'releaseYear' in entity:
                result_object['releaseYear'] = entity['releaseYear']['value']
            if 'genre' in entity:
                if not 'genre' in result_object:
                    result_object['genre'] = set()
                result_object['genre'].add(entity['genre']['value'])
            if 'publisher' in entity:
                if not 'publisher' in result_object:
                    result_object['publisher'] = set()
                result_object['publisher'].add(entity['publisher']['value'])
            if 'computingPlatform' in entity:
                if not 'computingPlatform' in result_object:
                    result_object['computingPlatform'] = set()
                result_object['computingPlatform'].add(entity['computingPlatform']['value'])
            if 'dbpediaObject' in entity:
                result_object['dbpediaObject'] = entity['dbpediaObject']['value']

        if 'type' in result_object:
            final_results_filtered[result] = final_results[result]

    final_results_filtered = json.dumps(final_results_filtered, default=set_default)
    final_results_filtered = json.loads(final_results_filtered)

    return JsonResponse(final_results_filtered)
