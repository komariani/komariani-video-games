from django.conf.urls import url
from .views import index, getVideoGames

#url for app
urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^games/$', getVideoGames, name='getVideoGames'),
]
