var clone_idx = 0;
var pagination = 1;

function duplicate() {
    var original = document.getElementById('accordionExample');
    var clone = original.cloneNode(true);
    clone.id = "item" + ++clone_idx;

    var card = clone.querySelector("#heading0");
    card.id = "heading" + clone_idx;
    card.setAttribute("data-target", "#collapse" + clone_idx);
    card.setAttribute("aria-controls", "collapse" + clone_idx);

    var collapse = clone.querySelector("#collapseOne");
    collapse.id = "collapse" + clone_idx;
    collapse.setAttribute("aria-labelledby", "heading" + clone_idx);
    collapse.setAttribute("data-parent", "#item" + clone_idx);
    
    $('#search-result').append(clone);
    clone.style.display = "block";
    return clone;
}

function removeURI(item) {  //remove the http thingy and take the name at the end
    var itemArray = item.split("/");
    var itemName = itemArray[itemArray.length - 1];
    return itemName;
}

function processGameDetail(filter, detail) { //process items to links and not links
    var i;
    var result = "";
    if (!Array.isArray(detail)) {
        var containURI = detail.includes('http');
        var detailItem = removeURI(detail);
        if (((filter != 'designer') && (containURI)) || (filter == 'year')) { //make it clickable except for designer
            result = '<a value=' + filter + '>' + detailItem + '</a>';
        } else {
            result = detailItem;
        }
    } else {
        for (i = 0; i < detail.length; ++i) {
            var containURI = detail[i].includes('http');
            var detailItem = removeURI(detail[i]);
            if (((filter != 'designer') && (containURI)) || (filter == 'year')) { //make it clickable except for designer
                result += '<a value=' + filter + '>' + detailItem + '</a>';
            } else {
                result += detailItem;
            }
            if (i != detail.length - 1) {
                result += ", ";
            }
        }
    }
    return result;
}


var filterNow = "";
var valueNow = "";
function searchGames(gameFilter, gameValue) {
    $('#search-result').empty();
    $('#spinner').show();
    $('#not-found').hide();
    $('#prev-page').hide();
    $('#next-page').hide();
    
    filterNow = gameFilter;
    valueNow = gameValue;
    $.ajax({
        type        : 'GET',
        url         : 'games?' + gameFilter + "=" + gameValue + "&page=" + pagination,
        dataType    : 'json',
        success: function(result){
            $('#spinner').hide();
            if (jQuery.isEmptyObject(result)) {
                if (pagination == 1) {
                    $('#not-found').text('No results found.');
                    $('#not-found').show();
                } else {
                    $('#prev-page').show();
                    $('#next-page').hide();
                    $('#not-found').text('No other data can be showed.');
                    $('#not-found').show();
                }
            }
            else {
                if (pagination == 1) {
                    $('#prev-page').hide();
                    $('#next-page').show();
                } else {
                    $('#prev-page').show();
                    $('#next-page').show();
                }
                $.each(result, function(i, item) {
                    var clone = duplicate();
                    var processedDetail = "";
                    clone.querySelector("#game-title").innerHTML = item.title;
                    
                    if (item.abstract == undefined) {
                        clone.querySelector("#abstract-row").remove()
                    } else {
                        clone.querySelector("#abstract-value").innerHTML = item.abstract;
                    }

                    if (item.releaseYear == undefined) {
                        clone.querySelector("#year-row").remove()
                    } else {
                        processedDetail = processGameDetail('year', item.releaseYear);
                        clone.querySelector("#year-value").innerHTML = processedDetail;
                    }

                    if (item.computingPlatform == undefined) {
                        clone.querySelector("#platform-row").remove()
                    } else {
                        processedDetail = processGameDetail('exact_platform', item.computingPlatform)
                        clone.querySelector("#platform-value").innerHTML = processedDetail;
                    }

                    if (item.genre == undefined) {
                        clone.querySelector("#genre-row").remove()
                    } else {
                        processedDetail = processGameDetail('exact_genre', item.genre)
                        clone.querySelector("#genre-value").innerHTML = processedDetail;
                    }

                    if (item.designer == undefined) {
                        clone.querySelector("#designer-row").remove()
                    } else {
                        processedDetail = processGameDetail('designer', item.designer)
                        clone.querySelector("#designer-value").innerHTML = processedDetail;
                    }

                    if (item.developer == undefined) {
                        clone.querySelector("#developer-row").remove()
                    } else {
                        processedDetail = processGameDetail('exact_developer', item.developer)
                        clone.querySelector("#developer-value").innerHTML = processedDetail;
                    }

                    if (item.mode == undefined) {
                        clone.querySelector("#mode-row").remove()
                    } else {
                        processedDetail = processGameDetail('exact_mode', item.mode)
                        clone.querySelector("#mode-value").innerHTML = processedDetail;
                    }

                    if (item.publisher == undefined) {
                        clone.querySelector("#publisher-row").remove()
                    } else {
                        processedDetail = processGameDetail('exact_publisher', item.publisher)
                        clone.querySelector("#publisher-value").innerHTML = processedDetail;
                    }

                    if (item.ignRating == undefined) {
                        clone.querySelector("#rating-row").remove()
                    } else {
                        clone.querySelector("#rating-value").innerHTML = item.ignRating;
                    }
                });
            }
        }
    });
}

function clearSearchBar() {
    $('#search_value').val("");
}

$(document).ready(function() {
    $("#accordionExample").hide();
    $('#spinner').hide();
    $('#not-found').hide();
    $('#prev-page').hide();
    $('#next-page').hide();

    $(".dropdown-menu a").click(function(){
        $('#dropdown-button').text($(this).text());
        $('#dropdown-button').val($(this).text());
      });
    
    $("#search-result").delegate(".card-header", "click", function() {
        var boolExpand = $(this).attr("aria-expanded");
        var objText = $(this).find('div.col-2');
        var infoBox = $(this).parent().find('div.collapse');
        if (boolExpand == "false") { //this means the accordion was closed then opens immediately when this function is called
            infoBox.collapse('show');
            objText.text('Less');
            $("<i class='fa fas fa-chevron-circle-up mx-3'></i>").appendTo(objText);
        }
        else {
            infoBox.collapse('hide');
            objText.text('More');
            $("<i class='fa fas fa-chevron-circle-down mx-3'></i>").appendTo(objText);
        }
    });

    var category = "contains_name";
    $('#category-list a').on('click', function(){
        category = $(this).attr('id');
    });

    $("#search-result").delegate("a", "click", function() {
        clearSearchBar();
        var exactFilter = $(this).attr('value');
        var exactItem = $(this).text();
        searchGames(exactFilter, exactItem);
    });


    $('#search-icon').click(function(){
        pagination = 1;
        var search_value = encodeURIComponent($('#search_value').val());
        searchGames(category, search_value);
        clone_idx = 0;
    });

    $("#search_value").on('keyup', function (e) {
        if (e.key === 'Enter' || e.keyCode === 13) {
            pagination = 1;
            var search_value = encodeURIComponent($('#search_value').val());
            searchGames(category, search_value);
            clone_idx = 0;
        }
    });

    $('#prev-page').click(function(){
        if (pagination != 1) {
            pagination = pagination - 1;
            searchGames(filterNow, valueNow);
            if ($("#not-found:contains('No other data can be showed.')").length > 0) {
                $('#not-found').text('No results found.');
            }
        }
    });

    $('#next-page').click(function(){
        if (!($("#not-found:contains('No other data can be showed.')").length > 0)) {
            pagination = pagination + 1;
            searchGames(filterNow, valueNow);
        }
    });
});